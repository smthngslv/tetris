from tetris.logic.tetraminos import LPiece

class Core:
    FIELD_ROWS    = 20
    FIELD_COLUMNS = 10

    def __init__(self):
        self.field = [[0 for x in range(Core.FIELD_COLUMNS)] for y in range(Core.FIELD_ROWS)]
        
        self._tetramino = LPiece([0, 0])

    def tick(self):
        pass

    def get_full_rows(self):
        pass
