from tetris.logic.tetraminos import Tetramino

class LPiece(Tetramino):
    MATRICES = [
        [[0, 1, 0], 
         [0, 1, 0], 
         [0, 1, 1]], 

        [[0, 0, 0], 
         [1, 1, 1], 
         [1, 0, 0]],

        [[1, 1, 0], 
         [0, 1, 0], 
         [0, 1, 0]],

        [[0, 0, 1], 
         [1, 1, 1], 
         [0, 0, 0]]]

    MATRICES_SIZE = (3, 3)

    def rotate_left(self):
        return False

    def rotate_right(self):
        return False