from abc import ABC, abstractmethod

class Tetramino(ABC):
    MATRICES      = None
    MATRICES_SIZE = None

    def __init__(self, position):
        self._mx       = self.MATRICES[0]
        self._position = [position[0] - self.MATRICES_SIZE[0] // 2, 
                          position[1] - self.MATRICES_SIZE[1] // 2]

    @abstractmethod
    def rotate_left(self):
        raise NotImplementedError()

    @abstractmethod
    def rotate_right(self):
        raise NotImplementedError()
    
    def __contains__(self, coords):
        return (0 <= coords[0] - self._position[0] < self.MATRICES_SIZE[0] and 
                0 <= coords[1] - self._position[1] < self.MATRICES_SIZE[1])

    def __getitem__(self, index):
        return self._mx[index[1] - self._position[1]][index[0] - self._position[0]]
