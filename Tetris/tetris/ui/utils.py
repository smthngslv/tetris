from tetris.config import Config

class Utils:
    @staticmethod
    def scale(*args, scale = Config.get('main', 'scale')):
        args = [arg * scale for arg in args]

        if len(args) == 1:
            return args[0]
        
        return args

