import pygame

from tetris import Config

from tetris.ui import Utils

from tetris.ui.graphics import Images

from tetris.ui.graphics.sprites import Sprite


class Image(Sprite):
    def __init__(self, image_name:str, position:tuple, **kwargs):
        """
        **kwargs:
            -name-       -type-  -default-
            image_scale  float   1.0
            hue_offset    int     0

        """

        self._image_name = image_name
        self._position   = position

        self._hue_offset  = 0
        self._image_scale = 1.0
        
        super().__init__(**kwargs)

    def _render(self):
        self.image = Images.get(self._image_name, self._image_scale, self._hue_offset)
        
        self.rect = self.image.get_rect()

        self.rect.x, self.rect.y = Utils.scale(*self._position)
    