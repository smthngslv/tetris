import pygame

from tetris import Config

from tetris.ui import Utils

from tetris.ui.graphics import Font

from tetris.ui.graphics.sprites import Sprite


class Label(Sprite):
    DEBUG_BACKGROUND = (0, 255, 0)

    def __init__(self, text:str, size:int, position:tuple, color:tuple, **kwargs):

        """
        **kwargs:
            -name-          -type-  -default-
            background      tuple   None
            align_box_size  int     None

        """

       

        self._text  = text
        self._size  = size
        self._color = color

        self._background = None
        self._align_box_size = None

        super().__init__(position = position, **kwargs)

    def set(self, **kwargs):
        if 'align_box_size' in kwargs:
             self._align_box_size = Utils.scale(*kwargs.pop('align_box_size'))
        
        if 'position' in kwargs:
            self._position = Utils.scale(*kwargs.pop('position'))

        super().set(**kwargs)
    
    def move(self, position = None, offset = None, ignore_scale = False):
        scale = 1 if ignore_scale else Config.get('main', 'scale')

        if position:
            position = (int(position[0] * scale), 
                        int(position[1] * scale))

            self.rect.x -= self._position[0] - position[0]
            self.rect.y -= self._position[1] - position[1]

            self._position = position

        if offset:
            offset = (int(offset[0] * scale), 
                      int(offset[1] * scale))

            self.rect.x += offset[0]
            self.rect.y += offset[1]

            self._position[0] += offset[0]
            self._position[1] += offset[1]


    def _render(self):
        self.image = Font.render(self._text, self._size, self._color, self._background)

        self.rect = self.image.get_rect()
        
        if self._align_box_size:
            text_x = self._align_box_size[0] // 2 - self.image.get_width()  // 2
            text_y = self._align_box_size[1] // 2 - self.image.get_height() // 2

            if self._background or Config.get('main', 'debug'):
                text_image = self.image
                
                self.image = pygame.surface.Surface(self._align_box_size).convert()

                self.image.fill(self._background if self._background else Label.DEBUG_BACKGROUND)

                self.image.blit(text_image, (text_x, text_y))

                self.rect = self.image.get_rect()

            else:
                self.rect.x += text_x
                self.rect.y += text_y

        self.rect.x += self._position[0]
        self.rect.y += self._position[1]
        
