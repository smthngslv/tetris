import pygame

from abc import ABC, abstractmethod

class Sprite(ABC, pygame.sprite.DirtySprite):
    def __init__(self, **kwargs):
        super().__init__(*kwargs.pop('groups', []))
        
        for key in ['dirty', 'blendmode', 'source_rect', 'visible', '_layer']:
            if key in kwargs:
                self.__dict__[key] = kwargs.pop(key)
        
        self.set(**kwargs)

    def draw(self, surface:pygame.surface.Surface):
        return surface.blit(self.image, self.rect)

    def set(self, update = True, **kwargs):
        for key in kwargs:
            if f'_{key}' not in self.__dict__:
                raise KeyError(f'Invalid parameter: {key}')

            self.__dict__[f'_{key}'] = kwargs[key]
        
        if self.dirty == 0 and update:
            self.dirty = 1

        self._render()

    def _render(self):
        pass
                

