import os
import io
import copy
import pygame

from tetris import Config

from tetris.ui import Utils

class Font:
    PATH = 'data/fonts/'
    DEBUG_BACKGROUND = (255, 0, 0)

    @staticmethod
    def initialize(pack = Config.get('main', 'pack')):
        with open(os.path.join(Font.PATH, f'{pack}.ttf'), 'rb') as file:
            Font._raw = io.BytesIO(file.read())

        Font._sizes = {}

    @staticmethod
    def render(text:str, size:int, color:tuple, background = None):
        # Caching
        if not size in Font._sizes:
            Font._sizes[size] = pygame.font.Font(copy.deepcopy(Font._raw), Utils.scale(size))
        
        if not background and Config.get('main', 'debug'):
            background = Font.DEBUG_BACKGROUND
            
        return Font._sizes[size].render(text, True, color, background)
