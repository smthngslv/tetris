import os
import copy
import glob
import pygame

from tetris import Config

from tetris.ui import Utils

class Images:
    PATH = 'data/images/'

    @staticmethod
    def initialize(pack  = Config.get('main', 'pack')):
        
        Images._images = {'__raw__': {}}
        
        # Load and scale.
        for path in (glob.glob(os.path.join(Images.PATH, pack, '*.png')) 
                     + glob.glob(os.path.join(Images.PATH, 'common', '*.png'))):
            # File name without extension.
            name = os.path.basename(path)[:-4]
            
            image = pygame.image.load(path)
            
            image = pygame.transform.smoothscale(image, Utils.scale(*image.get_size()))
            
            Images._images['__raw__'][name] = image

    @staticmethod
    def get(name:str, scale = 1.0, hue_offset = 0):
        scale = round(scale, 1)

        # Not converted yet.
        if not name in Images._images:
            # Scale 1.0, hue_offset 0.
            Images._images[name] = { 1.0: { 0: Images._images['__raw__'][name].convert_alpha() } }

        # Disctionary of cashed scales.
        scales = Images._images[name]

        # Scale not cashed.
        if not scale in scales:
            # Scale 1.0, hue_offset 0.
            original = scales[1.0][0]

            scaled = pygame.transform.smoothscale(original, (int(original.get_width() * scale),
                                                             int(original.get_height() * scale)))

            scales[scale] = {0: scaled}

        # Dictionary of cashed hue_offsets.
        offsets = scales[scale]

        # Offset not cashed.
        if not hue_offset in offsets:
            pass
        
        return offsets[hue_offset]

        
            

