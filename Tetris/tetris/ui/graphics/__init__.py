# Current level.
from tetris.ui.graphics.font   import Font
from tetris.ui.graphics.images import Images

Font.initialize()
Images.initialize()
