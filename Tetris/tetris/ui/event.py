import pygame

class Event:
    RESTART_WINDOW     = 2
    RESTART_ACTIVITY   = 0
    SWITCH_TO_ACTIVITY = 1

    def post(type_ = None, data = None, pygame_type = pygame.USEREVENT):
        pygame.event.post(pygame.event.Event(pygame_type, 
                                             {'type_': type_, 'data': data}))
