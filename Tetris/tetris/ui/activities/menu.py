import pygame

from tetris import Config

from tetris.ui import Event

from tetris.ui.activities import Activity

from tetris.ui.graphics.sprites import Image 

class Menu(Activity):
    def __init__(self, clock):
        super().__init__(clock, Config.get('layout', 'menu'))

    def rebuild(self):
        return Menu(self._clock)

    def handle_event(self, event):
        super().handle_event(event)

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LALT:
                Event.post(Event.SWITCH_TO_ACTIVITY, {'name': 'Game'})

