import pygame

from tetris import Config

from tetris.ui import Event

from tetris.ui.activities import Activity

from tetris.ui.activities.fragments import GameField

from tetris.ui.graphics.sprites import Image, Label 

from tetris.logic import Core

from tetris.ui.graphics import Images

class Game(Activity):
    def __init__(self, clock):
        super().__init__(clock, Config.get('layout', 'game'))
        
        self._game_field = GameField(Config.get('layout', 'game', 'config', 'cube_offset'))
        
        self.t = Images.get('test')

    def rebuild(self):
        return Game(self._clock)

    def handle_event(self, event):
        super().handle_event(event)

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LALT:
                Event.post(Event.SWITCH_TO_ACTIVITY, {'name': 'Menu'})

        
    #def draw(self, display):
    #    return self._game_field.draw(display)