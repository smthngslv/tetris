import pygame

from abc import ABC, abstractmethod

from tetris import Config

from tetris.ui import Event, Utils

from tetris.ui.graphics import sprites

from tetris.ui.graphics.sprites import Label


class Activity(ABC):
    DEBUG_TEXT_SIZE  = 20
    DEBUG_GRID_STEP  = 100
    DEBUG_TEXT_COLOR = (255, 255, 0)

    def __init__(self, clock:pygame.time.Clock, layout = None):
        self._clock = clock
        self._debug = False

        self._main_sprites_group  = pygame.sprite.LayeredDirty()
        
        self._background = pygame.surface.Surface(Utils.scale(*Config.get('layout', 'resolution'))).convert()
        self._background.fill(Config.get('main', 'background'))

        if Config.get('main', 'debug'):
            self._debug = True

            self._fps_label   = Label('0.00 fps', Activity.DEBUG_TEXT_SIZE, (0, 0), Activity.DEBUG_TEXT_COLOR, layer = 100)
            self._mouse_label = Label('(0, 0)',   Activity.DEBUG_TEXT_SIZE, (0, 0), Activity.DEBUG_TEXT_COLOR, layer = 101, dirty = 2)

            self._grid_top_label  = Label('0 px', Activity.DEBUG_TEXT_SIZE, (0, 0), Activity.DEBUG_TEXT_COLOR, layer = 100)
            self._grid_left_label = Label('0 px', Activity.DEBUG_TEXT_SIZE, (0, 0), Activity.DEBUG_TEXT_COLOR, layer = 100)

            self._main_sprites_group.add(self._fps_label)
            self._main_sprites_group.add(self._mouse_label)
            self._main_sprites_group.add(self._grid_top_label)
            self._main_sprites_group.add(self._grid_left_label)

            self.__update_grid_labels()
            self.__update_mouse_label()
        
        if layout:
            self._parse_layout(layout)

    @abstractmethod
    def rebuild(self):
        raise NotImplementedError()
    
    def draw(self, surface:pygame.surface.Surface):
        rects = self._main_sprites_group.draw(surface, self._background)
        
        if self._debug:  
            for x in range(0, (surface.get_width()  + Activity.DEBUG_GRID_STEP - 1) // Activity.DEBUG_GRID_STEP):
                rects.append(pygame.draw.line(surface, 
                                            Activity.DEBUG_TEXT_COLOR, 
                                            (x * Activity.DEBUG_GRID_STEP, 0), 
                                            (x * Activity.DEBUG_GRID_STEP, surface.get_height())))
                    
                self._main_sprites_group.repaint_rect(rects[-1])

            for y in range(0, (surface.get_height() + Activity.DEBUG_GRID_STEP - 1) // Activity.DEBUG_GRID_STEP):
                rects.append(pygame.draw.line(surface, 
                                            Activity.DEBUG_TEXT_COLOR, 
                                            (0, y * Activity.DEBUG_GRID_STEP),
                                            (surface.get_width(), y * Activity.DEBUG_GRID_STEP)))

                self._main_sprites_group.repaint_rect(rects[-1])

            self._fps_label.set(text = '%.2f fps' % self._clock.get_fps())
            
        return rects
    
    def handle_event(self, event:pygame.event.Event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_F12:
                debug = not Config.get('main', 'debug')
                
                pygame.mouse.set_visible(debug)
                Config.set(debug, 'main', 'debug')
                
                Event.post(Event.RESTART_ACTIVITY)

            elif event.key == pygame.K_F11:
                Config.set(not Config.get('main', 'full_screen'), 'main', 'full_screen')

                Event.post(Event.RESTART_WINDOW)

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == pygame.BUTTON_WHEELUP:
                if self._debug: 
                    Activity.DEBUG_GRID_STEP = min(Activity.DEBUG_GRID_STEP + 1, 500)

                    self.__update_grid_labels()

            if event.button == pygame.BUTTON_WHEELDOWN:
                if self._debug:  
                    Activity.DEBUG_GRID_STEP = max(Activity.DEBUG_GRID_STEP - 1,   2)
                    
                    self.__update_grid_labels()

        elif event.type == pygame.MOUSEMOTION:
            if self._debug: 
                self.__update_mouse_label()

    def _parse_layout(self, layout:dict):        
        for sprite in layout['background']:
            type_ = sprite.pop('type')

            sprites.__dict__[type_](**sprite).draw(self._background)
        
        for s in layout['static']:
            type_ = s.pop('type')

            self._main_sprites_group.add(sprites.__dict__[type_](**s))

    def __update_grid_labels(self):
        self._grid_top_label.set(text = ' %i px' % Activity.DEBUG_GRID_STEP)
        self._grid_top_label.move(position = (Activity.DEBUG_GRID_STEP, 0), ignore_scale = True)

        self._grid_left_label.set(text = ' %i px' % Activity.DEBUG_GRID_STEP)
        self._grid_left_label.move(position = (0, Activity.DEBUG_GRID_STEP), ignore_scale = True)

    def __update_mouse_label(self):
        pos = pygame.mouse.get_pos()

        self._mouse_label.set(text = '(%i, %i)' % pos)
        self._mouse_label.move(position = (pos[0] + 20, pos[1]), ignore_scale = True)
