import pygame

from abc import ABC, abstractmethod


class Fragment(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def draw(self, surface:pygame.surface.Surface):
        raise NotImplementedError();
