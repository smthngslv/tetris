import pygame

from tetris import Config

from tetris.ui.graphics import Images

from tetris.ui.graphics.sprites import Image

from tetris.ui.activities.fragments import Fragment

from tetris.logic import Core


class GameField(Fragment):
    def __init__(self, position:tuple):
        self._core = Core()

        self._cubes = pygame.sprite.LayeredDirty()
        
        cube_size = Config.get('layout', 'game', 'config', 'cube_size')

        for y in range(Core.FIELD_ROWS):
            for x in range(Core.FIELD_COLUMNS):
                cube = Image('cube_0', (x * cube_size[0] + position[0], y * cube_size[1] + position[1]))

                # Store position.
                cube.data = (x, y)

                self._cubes.add(cube)



    def draw(self, surface:pygame.surface.Surface):
        for cube in self._cubes:
            pass

        return self._cubes.draw(surface)

    def _update(self):
        for cube in self._cubes:
            position = cube.data

            if self._core.field[position[1]][position[0]] != 0:
                cube.image = ''#Нужное изображение.

            else:
                cube.visible = False