import os
import gc
import pygame

import tetris.ui.activities as activities

from tetris.ui     import Event, Utils
from tetris.config import Config

class Main:
    FPS = 60
    WINDOW_TITLE = "smthngslv's Tetris"
    #TRANSITIONS_SPEED = 0.5

    def __init__(self):
        pass

    def loop(self):
        display = self._build_window()

        #pygame.display.set_icon()
        pygame.display.set_caption(Main.WINDOW_TITLE)
        pygame.mouse.set_visible(Config.get('main', 'debug'))

        clock = pygame.time.Clock()

        mainloop = True
        activity = activities.Menu(clock)

        #old_activity = pygame.surface.Surface(display.get_size())
        #old_activity.set_alpha(255)

        while mainloop:
            for event in pygame.event.get():
                activity.handle_event(event)

                if event.type == pygame.QUIT:
                    mainloop = False 

                elif event.type == pygame.USEREVENT:
                    if event.type_ == Event.RESTART_ACTIVITY:
                        activity = activity.rebuild()
                    
                    elif event.type_ == Event.SWITCH_TO_ACTIVITY:
                        #old_activity.set_alpha(255); 

                        #old_activity.blit(display, (0, 0))

                        activity = activities.__dict__[event.data['name']](clock)

                    elif event.type_ == Event.RESTART_WINDOW:
                        display = self._build_window() 

                
            
            rects = activity.draw(display)

            #if old_activity.get_alpha():
                #display.blit(old_activity, (0, 0))

            #    old_activity.set_alpha(int(old_activity.get_alpha() * Main.TRANSITIONS_SPEED))

            #pygame.display.flip()
            
            pygame.display.update(rects)
            
            clock.tick(Main.FPS)
            
        pygame.quit()
        Config.save('main')

    def _build_window(self):
        flags      = pygame.DOUBLEBUF
        resolution = Utils.scale(*Config.get('layout', 'resolution'))

        flags |= (pygame.FULLSCREEN if Config.get('main', 'full_screen') else 0)

        return pygame.display.set_mode(resolution, flags)