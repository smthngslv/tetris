# Current level.
from tetris.config import Config

# For initialize.
import pygame; pygame.init()

# Read manualy. TODO: Maybe read from AppData|Registry.
cfg = Config.load('main')

Config.initialize(cfg['pack'])

del(cfg)
del(pygame)