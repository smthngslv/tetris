import json
import copy

class Config:
    _paths = {'main': 'config.json', 
              'layout': 'data/images/{pack}/layout.json'}
    
    _pack = 'default'
    _configs = {}

    @staticmethod
    def initialize(pack:str):
        Config._pack = pack
        
        for name in Config._paths:
            Config._configs[name] = Config.load(name)
        
    
    @staticmethod
    def get(*name:str):
        cfg = Config._configs

        for n in name:
            cfg = cfg[n]

        return copy.deepcopy(cfg)
    
    @staticmethod
    def set(value, *name:str):
        cfg = Config._configs

        for n in name[:-1]:
            cfg = cfg[n]

        cfg[name[-1]] = value

    @staticmethod
    def load(name:str):
        with open(Config._paths[name].replace('{pack}', Config._pack)) as file: 
            return json.load(file)

    @staticmethod
    def save(name:str):
        with open(Config._paths[name].replace('{pack}', Config._pack), 'w') as file: 
            json.dump(Config.get(name), file, indent = 2, sort_keys = True)